﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgenToPoint : MonoBehaviour {

    public GameObject target;
    private NavMeshAgent navAgent;
    public bool followTarget, followFromStart;

	void Start () {

        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (target == null)
        {

            target = GameObject.FindWithTag("Player");
        }

        if (target && followFromStart){
            navAgent.SetDestination(target.transform.position);

            
        }
		
	}
	
	void Update () {

        if (target && followTarget)
        {
            navAgent.SetDestination(target.transform.position);
        }
		
	}
}
