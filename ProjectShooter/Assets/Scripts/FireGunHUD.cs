﻿using UnityEngine;
using UnityEngine.UI;

public class FireGunHUD : MonoBehaviour {

    public FireGun firegun;
    public Text ammoText;
    public Image gunIcon;

    private void OnGUI()
    {
        ammoText.text = firegun.currentAmmo + "/" + firegun.gunData.magazineCapacity;
    }
}
